/*global define */

define([
  'backbone'
], function (Backbone) {
  'use strict';

  return Backbone.Model.extend({
    urlRoot: app.urls.base + '/subactivity/',
    validation: {
      name: {
        required: true,
        msg: 'Please enter a name'
      }
    },
    initialize: function () {
      this.set('userId', window.sessionStorage.userID);
    }
  });

});

