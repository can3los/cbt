/*global define */

define([
  'backbone'
], function (Backbone) {
  'use strict';

  return Backbone.Model.extend({
    urlRoot: app.urls.base + '/metric'
  });

});

