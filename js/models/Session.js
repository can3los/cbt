/*global define */

define([
    'backbone'
], function (Backbone) {
    'use strict';

    return Backbone.Model.extend({
        url: 'user/session',
        defaults: {
            userID: null,
            token: null
        }
    });
});

