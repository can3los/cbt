/*global define */
define([
    'backbone'
], function (Backbone) {
    'use strict';

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: app.urls.base + '/user/find',
        initialize: function () {
            this.set('_id', window.sessionStorage.userID);
        }
    });
});

