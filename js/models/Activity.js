/*global define */

define([
  'backbone'
], function (Backbone) {
  'use strict';

  return Backbone.Model.extend({
    urlRoot: app.urls.base + '/activity/',
    validation: {
      description: {
        required: true,
        msg: 'Please enter a description'
      }
    },
    initialize: function () {
      this.set('userId', window.sessionStorage.userID);
    }
  });

});

