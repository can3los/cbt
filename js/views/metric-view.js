/*global define */

define([
  'marionette',
  'templates',
  'underscore',
  'models/Metric'
], function (Marionette, templates, _, MetricModel) {
  'use strict';
  return Marionette.ItemView.extend({
    template: templates.pages.metricView,
    model: MetricModel
  });
});
