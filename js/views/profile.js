/*global define */

define([
    'marionette',
    'templates',
    'underscore',
    'models/User'
], function (Marionette, templates, _, UserModel) {
    'use strict';
    return Marionette.ItemView.extend({
        template: templates.pages.profile,
        initialize: function () {
            this.model = new UserModel();
            this.model.fetch();
            this.listenTo(this.model, 'sync', this.onSync, this);
        },
        onSync: function () {
            this.render();
        },
        onBeforeShow: function () {
//            console.log('onBeforeShow');
        },
        onShow: function () {
//            console.log('onShow');
        },
        onBeforeRender: function () {
//            console.log('onBeforeRender');
        },
        onRender: function () {
//            console.log('onRender');
        },
        templateHelpers: function () {
            return {model: this.model.toJSON()};
        }
    });
});
