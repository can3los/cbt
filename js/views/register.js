/*global define */

define([
  'marionette',
  'templates',
  'underscore'
], function (Marionette, templates, _) {
  'use strict';

  return Marionette.ItemView.extend({
    template: templates.register,
    ui: {
      username: '#cbt-username',
      password: '#cbt-password',
      password1: '#cbt-password1',
      email: '#cbt-email'
    },
    events: {
      'click #btn-register': 'do_register'
    },
    initialize: function () {
      _.bindAll(this, ['do_success']);
    },
    onRender: function () {
//      this.ui.username.val(_.uniqueId());
//      this.ui.password.val('123');
//      this.ui.password1.val('123');
//      this.ui.email.val('123@dot.com');
    },
    do_register: function (evt) {
      evt.preventDefault();
      var self = this, opts;
      var validate = this.do_validate();
      if (!validate.success) {
        app.commands.execute("app:dialog", {
          type: 'danger',
          title: 'Error!',
          description: 'Missining credentials'
        });
        return;
      }
      opts = {
        url: app.urls.base + '/user/create',
        type: 'POST',
        alerts: false,
        params: {
          username: this.$('input#cbt-username').val(),
          password: this.$('input#cbt-password').val(),
          first_name: this.$('input#cbt-firstname').val(),
          last_name: this.$('input#cbt-lastname').val(),
          email: this.$('input#cbt-email').val()
        }
      };
      var result = lib.callServer(opts);
      $.when(result).then(function (response, result, opts) {
        if (response.data) {
          self.do_success(response.data);
        } else {
          self.do_error(response);
        }
        return false;
      });
    },
    do_validate: function () {
      var validate = {
        success: false,
        error: null
      };
      var uValue = this.ui.username.val();
      var pValue = this.ui.password.val();
      if (!uValue || !pValue) {
        validate.error = 'Missing credentials';
      } else {
        validate.success = !validate.success;
      }
      return validate;
    },
    do_success: function (user) {
      app.vent.trigger('route:success_register', user);
    },
    do_error: function (err) {
      app.commands.execute("app:notify", {
        type: 'danger',
        title: 'Error!',
        description: err.error.message
      });
    }
  });
});
