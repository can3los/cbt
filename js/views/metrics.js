/*global define */

define([
  'marionette',
  'templates',
  'underscore',
  'collections/metrics',
  'views/metric-view'
], function (Marionette, templates, _, MetricsCollection, MetricItemView) {
  'use strict';
  return Marionette.CompositeView.extend({
    childView: MetricItemView,
    childViewContainer: "#metric-items",
    template: templates.pages.metrics,
    initialize: function () {
      this.collection = this.collection || new MetricsCollection();
      this.listenTo(this, 'fetch:success', function(collection) {
        this.collection = collection;
        this.render();
      })
      console.log('metrics', this.collection.toJSON());
    }
  });
});
