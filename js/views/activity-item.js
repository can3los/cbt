/*global define */

define([
    'marionette',
    'templates',
    'underscore',
    'models/Activity'
], function (Marionette, templates, _, ActivityModel) {
  'use strict';
  return Marionette.ItemView.extend({
    template: templates.pages.activityItemView,
    bindings: {
      '#cbt-activity-description': {
        observe: 'description'
      },
      '#cbt-activity-type': {
        observe: 'type'
      }
    },
    initialize: function () {
      this.parentView = this.options.parent;
      this.model = this.parentView.model || new ActivityModel();
      this.listenTo(this.parentView.model, 'sync', function (model) {
        this.model = model;
      }, this);
    },
    onBeforeShow: function () {
//      console.log(arguments);
    },
    onRender: function () {
      console.log(arguments);
//      this.stickit();
    },
  });

});
