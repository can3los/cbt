/*global define */

define([
    'marionette',
    'templates',
    'underscore',
    'models/Activity',
    'collections/Metrics',
    'views/activity-item',
    'views/metrics'
], function (Marionette, templates, _, ActivityModel, MetricsCollection, ActivityItemView, MetricsView) {
  'use strict';
  
  return Marionette.LayoutView.extend({
    template: templates.pages.activityUpdate,
    regions: {
      activityRegion: '#activity-region',
      metricsRegion: '#metrics-region'
    },
    // This callback will be called whenever a child is rendered or emits a `render` event
    childEvents: {
      render: function (childView) {
        console.log('now rendering', childView);
//        if (childView.model) {
//          var metricsView = this.getChildView('metricsRegion');
//          _.each(childView.model.get('_metrics'), function (metric, idx) {
//            metricsView.collection.add(metric);
//          });
//        }
      }
    },
    ui: {
      description: '.cbt-description',
      type: '.cbt-type'
    },
    events: {
      'click #cbt-update': 'onUpdate',
      'click #cbt-remove': 'onRemove',
      'click #cbt-createSub': 'onCreateSub',
      'click .sub-remove': 'onSubremove',
    },
    initialize: function () {
      var _id = this.options.params.id;
      this.model = new ActivityModel({
        id: _id
      });
      this.model.fetch();
      this.listenTo(this.model, 'sync', this._onSync, this);
    },
    onRender: function () {
//      this.activityRegion.show(new ActivityItemView());
//      this.metricsRegion.show(new MetricsView());
    },
    _onSync: function () {
//      var _metrics = this.model.get('_metrics');
//      this.activityRegion.show(new ActivityItemView({
//        model: this.model
//      }));
//      this.metricsRegion.show(new MetricsView({
//        collection: new MetricsCollection(_metrics)
//      }));

//      this.getChildView('activityRegion').trigger('fetch:success', this.model);
//      this.getChildView('metricsRegion').trigger('fetch:success', new MetricsCollection(this.model.get('_metrics')));
//      
//      this.render();
    },
    onBeforeShow: function () {
      this.showChildView('activityRegion', new ActivityItemView({
        parent: this
      }));
      this.showChildView('metricsRegion', new MetricsView({
        parent: this
      }));
    },
    onUpdate: function (evt) {
      evt.preventDefault();
      console.log('onUpdate', this.model.toJSON());
      this.model.save(null, {
        success: this.onAfterSaveDelete
      }, {validate: true});
    },
    templateHelpers: function () {
      return {
        _: _
      };
    },
    onRemove: function (evt) {
      evt.preventDefault();
      this.model.destroy({
        success: this.onAfterSaveDelete
      });
    },
    onDestroy: function () {
      this.unstickit(this.model);
    }
  });
});
