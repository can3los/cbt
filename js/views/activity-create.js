/*global define */

define([
  'marionette',
  'templates',
  'underscore',
  'models/Activity'
], function (Marionette, templates, _, ActivityModel) {
  'use strict';

  return Marionette.ItemView.extend({
    template: templates.pages.activityCreate,
    ui: {
      description: '#cbt-description',
      type: '#cbt-type'
    },
    events: {
      'click #cbt-save': 'onSave',
      'click #cbt-remove': 'onRemove'
    },
    bindings: {
      '#cbt-description': {
        observe: 'description'
      },
      '#cbt-type': {
        observe: 'type'
      }
    },
    initialize: function () {
      _.bindAll(this, ['onAfterSaveDelete']);
      this.model = new ActivityModel();
    },
    onRender: function () {
      this.stickit();
    },
    onSave: function (evt) {
      evt.preventDefault();
      this.model.save(null, {
        success: this.onAfterSaveDelete
      }, {validate: true});
    },
    onAfterSaveDelete: function (model) {
      this.options.parentView.collection.add(model);
      this.trigger('dialog:close');
    },
    templateHelpers: function () {
      return {
        title: 'Create activity'
      };
    },
    onRemove: function (evt) {
      evt.preventDefault();
      this.model.destroy({
        success: this.onAfterSaveDelete
      });
    },
    onDestroy: function () {
      this.unstickit(this.model);
    }
  });
});
