/*global define */

define([
  'marionette',
  'templates',
  'underscore',
  'collections/activities',
  'views/activity-view'
], function (Marionette, templates, _, ActivitiesCollection, ActivityItemView) {
  'use strict';
  return Marionette.CompositeView.extend({
    className: 'row',
    id: 'activities',
    childView: ActivityItemView,
    childViewContainer: "#activity-items",
    template: templates.pages.activities,
    events: {
      'click #create-activity': 'onCreate'
    },
    initialize: function () {
      this.collection = new ActivitiesCollection();
      this.collection.fetch();
      this.listenTo(this.collection, 'sync', this.onSync, this);
      this.listenTo(app.vent, 'collection:fetch', this.onFetch, this);

    },
    onFetch: function () {
      this.collection.fetch();
    },
    onSync: function () {
      this.render();
    },
    templateHelpers: function () {
      return {items: this.collection.toJSON()};
    },
    onCreate: function (evt) {
      evt.preventDefault();
      app.commands.execute("app:create:activity", this);
    }
  });
});
