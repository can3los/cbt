/*global define */

define([
    'marionette',
    'templates',
    'underscore',
    'models/Session'
], function (Marionette, templates, _, Session) {
    'use strict';

    return Marionette.ItemView.extend({
        template: templates.login,
        ui: {
            username: '#cbt-username',
            password: '#cbt-password'
        },
        events: {
            'click #btn-login': 'do_login'
        },
        do_login: function (evt) {
            evt.preventDefault();
            var opts;
            var validate = this.do_validate();
            if (!validate.success) {
                alert(validate.error);
                return false;
            }
            opts = {
                url: app.urls.base + '/user/login',
                type: 'POST',
                params: {
                    username: this.$('input#cbt-username').val(),
                    password: this.$('input#cbt-password').val()
                }
            };
            var result = lib.callServer(opts);
            $.when(result).then(function (response, result, opts) {
                var data = null;
                if (response.success && response.data && response.data.token !== null) {
                    data = response.data;
                    window.sessionStorage.accessToken = data.token;
                    window.sessionStorage.userID = app.userID = data._id;
                    app.vent.trigger('route:success_login');
                } else
                    app.vent.trigger('route:failure_login');
                return false;
            });
        },
        do_validate: function () {
            var validate = {
                success: false,
                error: null
            };
            var uValue = this.ui.username.val();
            var pValue = this.ui.password.val();
            if (!uValue || !pValue) {
                validate.error = 'Missing credentials';
            } else {
                validate.success = !validate.success;
            }
            return validate;
        }
    });
});
