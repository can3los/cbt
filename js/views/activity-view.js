/*global define */

define([
    'marionette',
    'templates',
    'underscore',
    'models/Activity'
], function (Marionette, templates, _, ActivityModel) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'a',
        template: templates.pages.activityView,
        model: ActivityModel,
        className: 'list-group-item',
        events: {
            'click': 'onEdit'
        },
        onEdit: function (evt) {
            evt.preventDefault();
            app.vent.trigger('route:activities_update', this.model.get('_id'));
        }
    });
});
