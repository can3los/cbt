/*global define */

define(function (require) {
  'use strict';

  return {
    pages: {
      home: require('tpl!templates/pages/home.html'),
      help: require('tpl!templates/pages/help.html'),
      profile: require('tpl!templates/pages/user/profile.html'),
      activities: require('tpl!templates/pages/activity/activities.html'),
      activityView: require('tpl!templates/pages/activity/activity-view.html'),
      activityItemView: require('tpl!templates/pages/activity/activity-item.html'),
      activityCreate: require('tpl!templates/pages/activity/activity-create.html'),
      activityUpdate: require('tpl!templates/pages/activity/activity-update.html'),
      metrics: require('tpl!templates/pages/metric/metrics.html'),
      metricView: require('tpl!templates/pages/metric/metric-view.html')
    },
    login: require('tpl!templates/pages/user/login.html'),
    register: require('tpl!templates/pages/user/register.html'),
    page: require('tpl!templates/page.html'),
    menuItem: require('tpl!templates/menuItem.html'),
    footer: require('tpl!templates/footer.html')
  };
});

