/*global define */

define([
  'backbone',
  'models/Subactivity'
], function (Backbone, Subactivity) {
  'use strict';
  return Backbone.Collection.extend({
    url:app.urls.base + '/subactivities',
    model: Subactivity
  });
});
