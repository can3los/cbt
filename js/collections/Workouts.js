/*global define */

define([
    'backbone',
    'models/Workout'
], function (Backbone, Workout) {
    'use strict';

    return Backbone.Collection.extend({
        url: app.urls.base + '/workout/list',
        model: Workout
    });
});
