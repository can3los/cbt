/*global define */

define([
  'backbone',
  'models/Activity'
], function (Backbone, Activity) {
  'use strict';

  return Backbone.Collection.extend({
    url:app.urls.base + '/activities',
    model: Activity
  });
});
