/*global define */

define([
  'backbone',
  'models/Metric'
], function (Backbone, Metric) {
  'use strict';

  return Backbone.Collection.extend({
    url:app.urls.base + '/metric',
    model: Metric
  });
});
