require.config({
    paths: {
        underscore: '../bower_components/underscore/underscore',
        backbone: '../bower_components/backbone/backbone',
        stickit: '../bower_components/backbone/backbone.stickit',
        routeFilter: '../bower_components/backbone/backbone.routefilter',
        backboneValidation: '../bower_components/backbone/backbone-validation-amd',
        marionette: '../bower_components/backbone.marionette/lib/backbone.marionette',
        moment: '../bower_components/moment/moment.min',
        jquery: '../bower_components/jquery/jquery',
        localStorage: '../bower_components/backbone.localStorage/backbone.localStorage',
        tpl: 'lib/tpl',
        bootstrap: 'lib/bootstrap.min',
        bootstrapInputTags: '../bower_components/bootstrap-tagsinput/bootstrap-tagsinput',
        bootstrapMultiSelect: 'lib/bootstrap-multiselect/js/bootstrap-multiselect'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            exports: 'Backbone',
            deps: ['jquery', 'underscore']
        },
        marionette: {
            exports: 'Backbone.Marionette',
            deps: ['backbone']
        },
        moment: {
            exports: 'Moment',
            deps: ['jquery']
        },
        bootstrap: {
            deps: ['jquery']
        },
        bootstrapMultiSelect: {
            deps: ['bootstrap']
        },
        bootstrapInputTags: {
            deps: ['bootstrap']
        },
        stickit: {
            deps: ['backbone']
        },
        routeFilter: {
            deps: ['backbone']
        },
        backboneValidation: {
            deps: ['backbone']
        }

    },
    waitSeconds: 15
});

require([
    'app',
    'lib',
    'moment',
    'jquery',
    'routers/index',
    'routeFilter',
    'bootstrap',
    'stickit',
    'bootstrapInputTags'
], function (app, lib, moment, $, Router) {
    'use strict';

    Backbone.$.ajaxSetup({
        beforeSend: function () {
            //show indicator
        },
        complete: function (x, error) {
            //hide indicator
            if (x.status == 401) {
                app.vent.trigger('user:login');
            }
        },
        error: function (x, status, error) {
            if (x.status == 401) {
                alert("Sorry, your session has expired. Please login again to continue");
                app.vent.trigger('user:login');
            }
            else {
                alert("An error occurred: " + status + "nError: " + error);
            }
        }
    });

    /*
     * Store a version of Backbone.sync to call from the
     * modified version we create
     */
    var backboneSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {
        /*
         * The jQuery `ajax` method includes a 'headers' option
         * which lets you set any headers you like
         */
        options.headers = {
            /*
             * Set the 'Authorization' header and get the access
             * token from the `auth` module
             */
            'Authorization': 'Bearer ' + window.sessionStorage.accessToken
        };

        /*
         * Call the stored original Backbone.sync method with
         * extra headers argument added
         */
        backboneSync(method, model, options);
    };

    app.start();
});
