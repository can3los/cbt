/*global define */

define([
  'backbone',
  'marionette',
  'controllers/index'
], function (Backbone, Marionette, Controller) {
  'use strict';

  var Router = Backbone.Router.extend({
    routes: {
      '*actions': 'do_action',
      'logout': 'do_logout'
    },
    do_logout: function () {
      delete window.sessionStorage.accessToken;
      delete window.sessionStorage.userID;
      this.navigate('{"cls":"login"}', {trigger: true});
    },
    do_action: function (actions) {
      var self = this;
      require(['app'], function (app) {
        var url, params;

        if (!actions || _.isNull(actions) || _.isUndefined(actions)) {
          actions = app.urls.home;
        }

        //get target url;
        url = lib.decode(actions);

        if ((url.cls != 'login') && (url.cls != 'register') && !app.isAuth()) {
          app.vent.trigger('route:login');
          return;
        }

        if (url.cls == 'logout') {
          self.do_logout();
          return;
        }

        //load the class
        require(["views/" + url.cls], function (View) {
          app.vent.trigger('view:show', View, _.pick(url, 'params'));
        });
      });
    },
    before: {
      '*actions': function (route, url) {
        var clsObj;
        if (!app.isAuth()) {
          url[0] = app.urls.login;
        } else {
          clsObj = lib.decode(url[0]);
          if (_.isNull(clsObj))
            app.vent.trigger('route:home');
          if (clsObj.cls == 'logout') {
            app.vent.trigger('route:logout');
            return;
          }
        }
      }
    }
  });

  return Router;

  /**
   * @type Marionette.AppRouter
   */
//  var Controller = new Controller();
//
//  return Marionette.AppRouter.extend({
//    controller: Controller,
//    appRoutes: {
//      '*actions': 'do_action'
//    }
//  });
});

