/*global define */

define([
  'backbone',
  'marionette',
  'routers/index',
  'regions/notification',
  'regions/dialog',
  'collections/Nav',
  'views/MenuView',
  'views/Footer'
], function (Backbone, Marionette, Router, NotifyRegion, DialogRegion, Nav, MenuView, Footer) {
  'use strict';

  var app = new Marionette.Application();

  app.urls = {
    home: '{"cls":"home"}',
    login: '{"cls":"login"}',
    register: '{"cls":"register"}',
    base: 'http://127.0.0.1:3007'
  };

  app.statics = null;

  app.pages = new Nav([
    {title: 'Home', name: 'home', active: true},
    {title: 'Activities', name: 'activities'},
    {title: 'Metrics', name: 'metrics'},
    {title: 'Profile', name: 'profile'},
    {title: 'Logout', name: 'logout'}
  ]);

  app.AppMenu = new MenuView({collection: app.pages});
  app.AppPublicMenu = new MenuView({collection: app.publicPages});

  app.isAuth = function () {
    var auth = false, token = window.sessionStorage.accessToken;
    if (!_.isUndefined(token) && !_.isNull(token)) {
      auth = !auth;
    }
    return auth;
  };

  app.addRegions({
    menu: '#main-nav',
    main: '#main',
    footer: '#footer',
    notification: {
      selector: ".system-message",
      regionType: NotifyRegion
    },
    dialog: DialogRegion
  });

  app.addInitializer(function () {
    //TODO
  });

  app.on('start', function (options) {
    this.router = new Router();
    if (Backbone.history)
      Backbone.history.start({pushState: false});
  });

  app.vent.on('route:home', function () {
    app.router.navigate('{"cls":"home"}', {trigger: true});
  });

  app.vent.on('route:success_login', function () {
    app.router.navigate('{"cls":"home"}', {trigger: true});
  });

  app.vent.on('route:success_register', function (user) {
    app.router.navigate('{"cls":"login"}', {trigger: true});
  });

  app.vent.on('route:activities_list', function (model) {
    app.router.navigate('{"cls":"activities"}', {trigger: true});
  });

  app.vent.on('route:activities_update', function (modelId) {
    app.router.navigate('{"cls":"activity-update", "params": {"id":"' + modelId + '"} }', {trigger: true});
  });


  app.vent.on('route:logout', function () {
    delete window.sessionStorage.accessToken
    delete window.sessionStorage.userID;
    app.router.navigate('{"cls":"login"}', {trigger: true});
  });

  app.vent.on('route:login', function () {
    app.router.navigate('{"cls":"login"}', {trigger: true});
  });

  app.vent.on('menu:activate', function (activePageModel) {
    app.AppMenu.collection.findWhere({active: true}).set('active', false);
    activePageModel.set('active', true);
    app.AppMenu.render();
  });

  app.commands.setHandler("app:create:activity", function (view) {
    require(['views/activity-create'], function (ActivityInputView) {
      app.dialog.show(new ActivityInputView({
        parentView: view
      }));
    });
  });
  
  app.commands.setHandler("app:create:subactivity", function (view) {
    require(['views/subactivity-create'], function (SubactivityInputView) {
      app.dialog.show(new SubactivityInputView({
        parentView: view
      }));
    });
  });

  app.commands.setHandler("app:update:activity", function (modelId) {
    require(['views/activity-update'], function (ActivityUpdateView) {
      app.vent.trigger('view:show', ActivityUpdateView, {
        id: modelId
      });
    });
  });

  app.commands.setHandler("app:add:subactivity", function (view) {
    require(['views/subactivity-input'], function (SubactivityInputView) {
      app.dialog.show(new SubactivityInputView({
        parentView: view
      }));
    });
  });

  app.commands.setHandler("app:notify", function (jsonData) {
    require(['views/NotificationView'], function (NotifyView) {
      app.notification.show(new NotifyView({
        model: new Backbone.Model(jsonData)
      }));
    });
  });

  /**
   * Show view handler
   */
  app.vent.on('view:show', function (View, params) {
    var auth = app.isAuth();
    if (auth) {
      app.menu.show(app.AppMenu);
    }
    app.main.show(new View(params));
  });

  return window.app = app;
});
