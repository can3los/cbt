var Path = require('path');
var fs = require('fs');
var Config = require('./config');

//sweet hapi
var Hapi = require('hapi');

//auth tools
var hapiAuthJwt = require('hapi-auth-jwt2');
var jwt = require('jsonwebtoken');

//development tool
var nomo = require('node-monkey').start();

//load mongoose
var Mongoose = require('mongoose');

//error library
var Boom = require('boom');

//load user model
var User = require('./models/user-model');

//debug mode
Mongoose.set('debug', false);

//database connection /{dbname}
Mongoose.connect('mongodb://localhost/cbt_db'); 

//server config
var server = new Hapi.Server(); 

//make the connection
server.connection({port: Config.server.port, routes: {cors: true}}); 

var validate = function (auth, request, callback) {
    var user;
    if (!auth.userID)
        return callback(null, false);
    else {
        user = User.find({
            _id: auth.userID
        }, function (err, user) {
            return callback(null, true);
        });
    }
};

server.register(hapiAuthJwt, function () {
    server.auth.strategy('jwt', 'jwt', true, {
        key: Config.key.privateKey,
        validateFunc: validate
    });
    require('./routes/users-routes')(server);
    require('./routes/workouts-routes')(server);
    require('./routes/activities-routes')(server);
    require('./routes/metrics-routes')(server);
});

//start the server
server.start(function () {
    console.log('Server running at:', server.info.address + ':' + server.info.port);
});
