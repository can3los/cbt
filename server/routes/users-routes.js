var Controller = require('../controllers/user-controller');

module.exports = function (server) {
  'use strict';

  server.route({
    method: 'POST',
    path: '/user/login',
    config: {
      auth: false
    },
    handler: Controller.do_login
  });

  server.route({
    method: 'GET',
    path: '/user/list',
    config: {
      auth: false
    },
    handler: Controller.do_list
  });

  server.route({
    method: 'GET',
    path: '/user/find/{id?}',
    config: {
      auth: 'jwt'
    },
    handler: Controller.do_find
  });

  server.route({
    method: 'POST',
    path: '/user/create',
    config: {
      auth: false
    },
    handler: Controller.do_create
  });

};