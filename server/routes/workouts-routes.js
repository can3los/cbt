var Config = require('../config');
var Controller = require('../controllers/workout-controller');

module.exports = function (server) {
    'use strict';
    server.route({
        method: 'GET',
        path: '/workout/list',
        config: {
            auth: 'jwt'
        },
        handler: Controller.do_find
    });

};