var Config = require('../config');
var Controller = require('../controllers/activity-controller');

module.exports = function (server) {
    'use strict';

    server.route({
        method: ['GET', 'POST', 'PUT', 'DELETE'],
        path: '/activity/{id?}',
        config: {
            auth: 'jwt'
        },
        handler: function (request, reply) {
            switch (request.method) {
                case 'get':
                    Controller.do_find(request, reply);
                    break;
                case 'post':
                    Controller.do_create(request, reply);
                    break;
                case 'put':
                    Controller.do_update(request, reply);
                    break;
                case 'delete':
                    Controller.do_delete(request, reply);
                    break;
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/activities',
        config: {
            auth: 'jwt'
        },
        handler: Controller.do_list
    });


//  
//  server.route({
//    method: 'POST',
//    path: '/activity/create',
//    config: {
//      auth: 'jwt'
//    },
//    handler: Controller.do_create
//  });
//  
//  server.route({
//    method: ['GET','DELETE'],
//    path: '/activity/find/{actID}',
//    config: {
//      auth: 'jwt'
//    },
//    handler: Controller.do_find
//  });


};