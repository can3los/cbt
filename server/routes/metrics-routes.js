var Config = require('../config');
var Controller = require('../controllers/metric-controller');

module.exports = function (server) {
  'use strict';

  server.route({
    method: ['GET', 'POST', 'PUT', 'DELETE'],
    path: '/metric/{id?}',
    config: {
      auth: 'jwt'
    },
    handler: function (request, reply) {
      switch (request.method) {
        case 'get':
          Controller.do_find(request, reply);
          break;
      }
    }
  });
  
};