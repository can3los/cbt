var Mongoose = require('mongoose');
var shortid = require('shortid');

//define schema
var ActivitySchema = new Mongoose.Schema({
    _userId: {type: Mongoose.Schema.Types.ObjectId, ref: "User", required: true},
    _metrics: [{type: Mongoose.Schema.Types.ObjectId, ref: "Metric"}],
    description: {type: String, required: true},
    type: {type: String, required: true},
    created_at: {type: Date, default: new Date()}
});

module.exports = Mongoose.model('Activity', ActivitySchema);