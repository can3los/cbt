var Mongoose = require('mongoose');
var shortid = require('shortid');

//define schema
var WorkoutSchema = new Mongoose.Schema({
  _userId: {type: String, required: true},
  _activities: [{type: Mongoose.Schema.Types.ObjectId, ref: "Activity"}],
  description: {type: String, required: true},
  created_at: {type: Date, default: new Date()}
});

module.exports = Mongoose.model('Workout', WorkoutSchema);