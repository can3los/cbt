var Mongoose = require('mongoose');
var shortid = require('shortid');

//define schema
var MetricSchema = new Mongoose.Schema({
    description: {type: String, required: true},
    type: {type: String, required: true},
    created_at: {type: Date, default: new Date()}
});

module.exports = Mongoose.model('Metric', MetricSchema);