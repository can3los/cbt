var Mongoose = require('mongoose');
var shortid = require('shortid');

//define schema
var UserSchema = new Mongoose.Schema({
  username: {type: String, required: true},
  password: {type: String, required: true},
  email: {type: String, required: false},
  first_name: {type: String, required: false},
  last_name: {type: String, required: false},
  date_birth: {type: Date, required: false},
  gender: {type: String, required: false},
  active: {type: Boolean, default: true},
  created_at: {type: Date, default: new Date()}
});

module.exports = Mongoose.model('User', UserSchema);