module.exports = {
  validate: function (data) {
    var errors = {};
    if (data.username == '') {
      errors.field = 'username';
      errors[errors.field] = {
        message: 'Username is mising'
      };
    }
    if (data.password == '') {
      errors.field = 'password';
      errors[errors.field] = {
        message: 'Password is mising'
      };
    }
    return errors;
  }
};