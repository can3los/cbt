var Config = require('../config');
var Boom = require('boom');
var Workout = require('../models/workout-model');

module.exports = {
     do_list: function (request, reply) {
        Workout.find({
            _userId: request.auth.credentials.userID
        }).sort({'created_at': -1}).exec(function (err, workouts) {
            if (err)
                reply(err);
            else
                reply(workouts);
        });
    },
    do_find: function (request, reply) {
        Workout.findOne({
            _id: request.params.id,
            _userId: request.auth.credentials.userID
        }).exec(function (err, workout) {
            if (err)
                reply(err);
            else {
                reply(workout);
            }
        });
    },
    do_create: function (request, reply) {
        var workout = new Workout({
            name: request.payload.name,
            _userId: request.auth.credentials.userID
        });
        workout.save(function (err, workout) {
            if (err) {
                reply(err);
            } else {
                reply(workout);
            }
        });
    }
};