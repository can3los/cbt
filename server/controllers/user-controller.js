var Config = require('../config');
var Jwt = require('jsonwebtoken');
var privateKey = Config.key.privateKey;
var ObjectId = require('mongoose').Types.ObjectId;
var Boom = require('boom'); //error library

var User = require('../models/user-model');
//var Validator = require('../models/user-validator');

module.exports = {
    do_create: function (request, reply) {
//    var user;
        //validation = Validator.validate(request.payload);
        var user = new User({
            username: request.payload.username,
            password: request.payload.password,
            password1: request.payload.password1,
            email: request.payload.email,
            first_name: request.payload.first_name,
            last_name: request.payload.last_name,
            date_birth: request.payload.dob,
            gender: request.payload.gender,
            active: request.payload.active || true
        });
        user.save(function (err, user) {
            console.log(user);
            if (err) {
                reply(err);
            } else {
                reply({success: true, data: user});
            }
        });
    },
    do_login: function (request, reply) {
        var username = request.payload.username;
        var password = request.payload.password;
        User.findOne({
            username: username,
            password: password
        }, function (err, user) {
            if (err) {
                reply({success: false, message: err});
            } else {
                if (user !== null) {
                    var tokenData = {
                        name: user.username,
                        userID: user._id,
                        created: new Date().getTime()
                    };
                    var res = {
                        success: true,
                        data: {
                            name: user.username,
                            _id: user._id,
                            token: Jwt.sign(tokenData, privateKey)
                        }
                    };
                    reply(res);
                } else
                    reply({success: false, message: 'User not found.'});
            }
        });
    },
    do_list: function (request, reply) {
        User.find().sort({'created_at': -1})
                .exec(function (err, users) {
                    if (err) {
                        reply({
                            success: false,
                            message: err
                        });
                    } else
                        reply({
                            success: true,
                            data: users
                        });
                });
    },
    do_find: function (request, reply) {
//        var paramName = request.params.pname ? encodeURIComponent(request.params.pname) : null;
        var userID = request.auth.credentials.userID;
        User.findOne({
            "_id": userID
        }, {
            username: 1,
            first_name: 1,
            last_name: 1,
            date_birth: 1,
            gender: 1,
            active: 1
        }).lean().exec(function (err, user) {
            if (err)
                reply(Boom.badRequest('Model error'));
            else {
                reply(user);
            }
        });
    }


};