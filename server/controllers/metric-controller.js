var Config = require('../config');
var Boom = require('boom');
var Metric = require('../models/metric-model');

module.exports = {
     do_list: function (request, reply) {
        Metric.find({
            _userId: request.auth.credentials.userID,
            _type: request.params.type
        }).sort({'created_at': -1}).exec(function (err, activities) {
            if (err)
                reply(err);
            else
                reply(activities);
        });
    },
    do_find: function (request, reply) {
        Metric.findOne({
            _id: request.params.id,
            _userId: request.auth.credentials.userID
        }).lean().exec(function (err, metric) {
            if (err)
                reply(err);
            else {
                reply(metric);
            }
        });
    }
};