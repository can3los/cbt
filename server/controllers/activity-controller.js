var Config = require('../config');
var Boom = require('boom'); //error library

var Activity = require('../models/activity-model');

module.exports = {
  do_create: function (request, reply) {
    console.log(request.payload);
    var activity = new Activity({
      description: request.payload.description,
      type: request.payload.type,
      _userId: request.auth.credentials.userID
    });
    activity.save(function (err, activity) {
      if (err) {
        reply(err);
      } else {
        reply(activity);
      }
    });
  },
  do_list: function (request, reply) {
    Activity.find({
      _userId: request.auth.credentials.userID
    }).populate('_metrics').sort({'created_at': -1}).exec(function (err, activities) {
      if (err)
        reply(err);
      else {
        reply(activities);
      }
    });
  },
  do_find: function (request, reply) {
    Activity.findOne({
      _id: request.params.id,
      _userId: request.auth.credentials.userID
    }).populate('_metrics').lean().exec(function (err, activity) {
      if (err)
        reply(err);
      else {
        reply(activity);
      }
    });
  },
  do_update: function (request, reply) {
    Activity.findOne({
      _id: request.payload._id,
      _userId: request.auth.credentials.userID
    }).exec(function (err, activity) {
      if (err)
        reply(err);
      else {
        //update activity
        activity.description = request.payload.description;
        activity.type = request.payload.type
        activity.save(function (err) {
          if (err)
            reply(err);
          else
            reply(activity);
        });
      }
    });
  },
  do_delete: function (request, reply) {
    Activity.findOne({
      _id: request.params.id,
      _userId: request.auth.credentials.userID
    }).exec(function (err, activity) {
      if (err)
        reply(err);
      else {
        activity.remove(function (err, result) {
          reply(result);
        })
      }
    });
  }
};