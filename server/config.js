module.exports = {
  server: {
    host: '10.82.20.200',
    port: 3007
  },
  database: {
    host: '10.82.20.200',
    port: 27017,
    db: 'cbt_db',
    username: '',
    password: ''
  },
  key: {
    privateKey: '0can3los0',
    tokenExpiry: 1 * 30 * 1000 * 60 //1 hour
  },
  admin: {
    username: "admin",
    password: "admin"
  },
  email: {
    accountName: "MrNull",
    verifyEmailUrl: "verifyEmail"
  }
};